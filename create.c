#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

int main(int argc, char **argv) {
    uint32_t faded = 0xfadedbee;
    FILE *fd = fopen(argv[1], "wb+");
    if (fd == NULL){
        printf("could not open");
        exit(1);
    }
    uint32_t N = atoi(argv[2]);
    uint32_t K = N / 1024;
    uint32_t block_size = 4096;
    if (N % 1024){
        K++;
    }
    uint32_t start = K + 1;
    long total_size= 4096 * N;

    fseek(fd, total_size, SEEK_SET);
    fputc('\0', fd);

    fseek(fd, 0, SEEK_SET); 
    fwrite(&faded, sizeof(faded), 1, fd); // magic number

    fwrite(&N, sizeof(N), 1,  fd); // total num of blocks in file sys
    fwrite(&K, sizeof(K), 1,  fd); // num of blocks in FAT

    fwrite(&block_size, 1, sizeof(block_size), fd); // block size
    fwrite(&start, sizeof(start), 1,  fd); // starting block of root
    
    //go to end of superblock, where fat block starts
    fseek(fd, 4096, SEEK_SET);

    int32_t negativeOne = -1;
    fwrite(&negativeOne, sizeof(negativeOne), 1, fd); 
    
    //write -1 for fat entries for fatblock itself.
    int j = 0;
    for (; j < K; j++){

        fwrite(&negativeOne, sizeof(negativeOne), 1, fd);  
    }

    fseek(fd, 4096, SEEK_SET); //go to start of FAT table
    fseek(fd, start * sizeof(uint32_t), SEEK_CUR); //offset K integers from FAT table to go to root fat entry

    int32_t negativeTwo = -2; //initialize fat entry of root to negative 2
    fwrite(&negativeTwo, sizeof(negativeTwo), 1, fd); 

    fclose(fd);
}
