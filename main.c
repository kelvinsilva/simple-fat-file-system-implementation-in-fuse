
#define FUSE_USE_VERSION 30


#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <assert.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdlib.h>


static int fd; //represents disk fd
static int currOffset; //represents where we are in the hard disk file


static uint32_t* fatPointer;

static int fat_opendir_helper(const char* path, struct fuse_file_info* fi, int flag, int* startBlockReturn);

static void cutLastPath(char * returnHere, const char * source, char * lastPathName); 

int findFreeFATEntry(uint32_t* fatPtr);

void writeFatTableToDisk();

static int findFreeSpaceInDirectory(int fileOffset, int startBlock, int* fileOffsetOut);
static int fat_open(const char *path, struct fuse_file_info *fi);

struct fatDirectory{

    char fileName[24];//total 64 characters
    uint64_t creationTime;
    uint64_t modificationTime;
    uint64_t accessTime;
    uint32_t fileLength;
    uint32_t startBlock;
    uint32_t flags;
    uint32_t unused;
};

struct superBlock{

    uint32_t magicNumber;
    uint32_t N;
    uint32_t K;
    uint32_t blockSize;
    uint32_t rootBlock;
} super;



int getDirMetadata(const char * path, struct fatDirectory * returnHere, int flag, int *offset){
   char * pathWithoutFileName = malloc(strlen(path) + 1);
   char pathWithName[24];
   memset(pathWithName, 0, 24);
   memset(pathWithoutFileName, 0, strlen(path) + 1);

   //get the path, and split filename. ex /usr/local/file.txt, and /usr/local/ and file.txt
   cutLastPath(pathWithoutFileName, path, pathWithName);
  
   struct fuse_file_info fi;
   //subdirectories will always be directories
   // ex: /folder/folder1/file.txt, when flag is 0, looking for file.txt you will always have folders ast pathWithouName.
   // so when path is cut, always call opendir helper with flag of 1
   fat_opendir_helper(pathWithoutFileName, &fi, 1, NULL);
    
   int nextIndex = fi.fh / 4096; //get start block
   int bytesTraversed = 0;
   struct fatDirectory tempFdir;


   while( (nextIndex != -2)){

       while (bytesTraversed < 4096 ){
            
           //bytesTraversed += fread(&tempFdir, sizeof(struct fatDirectory), 1, tempFD);
            bytesTraversed += read(fd, &tempFdir, sizeof(struct fatDirectory));

           if ( (strcmp(&tempFdir.fileName[0], pathWithName) == 0) && ( tempFdir.flags == flag )  ){
               //file found
                strcpy(&returnHere->fileName[0], &tempFdir.fileName[0]);
                returnHere->creationTime = tempFdir.creationTime;
                returnHere->modificationTime = tempFdir.modificationTime;
                returnHere->accessTime = tempFdir.accessTime;
                returnHere->fileLength = tempFdir.fileLength;
                returnHere->startBlock = tempFdir.startBlock;
                returnHere->flags = tempFdir.flags;
                returnHere->unused = tempFdir.unused;
                int sz = sizeof(struct fatDirectory);
                *offset = lseek(fd, -sz, SEEK_CUR);
                return 0;
           }
           
       }//at the end of this loop, we traversed 4096, so go to next block
       nextIndex = fatPointer[nextIndex];
       //make sure all indexes referenced from start of root block.
       //fseek(tempFD, (nextIndex + super.K + 1) * 4096, SEEK_SET);    
       //fseek(tempFD, (k+1)* 4096 + 1, SEEK_SET);
       lseek(fd, 4096 * nextIndex, SEEK_SET); //go to start of next block
       //decide later? do sme whiteboarding. or if you are super sure, choose one

       bytesTraversed = 0; 
   }
    return -1;

}

//return start block of a certain directory matchin fileName
int findDirectory(uint32_t* fatTable, char* fileName, int startBlock, int flag){
    
   int bytesTraversed = 0;
   //seek the startblock
  //` fseek(tempFD, (startBlock * 4096), SEEK_SET);
   lseek(fd, startBlock * 4096, SEEK_SET); //go to the startblock of a directory.

   struct fatDirectory tempFdir;
   memset(&tempFdir, 0, sizeof(struct fatDirectory));
   
       
   int nextIndex = startBlock; 

   while( (nextIndex != -2)){

       while (bytesTraversed < 4096 ){
            
           //bytesTraversed += fread(&tempFdir, sizeof(struct fatDirectory), 1, tempFD);
            bytesTraversed += read(fd, &tempFdir, sizeof(struct fatDirectory));

           if ( (strcmp(&tempFdir.fileName[0], fileName) == 0) && ( tempFdir.flags == flag )  ){
               //file found
               return tempFdir.startBlock; 
           }
           
       }//at the end of this loop, we traversed 4096, so go to next block
       nextIndex = fatTable[nextIndex];
       //make sure all indexes referenced from start of root block.
       //fseek(tempFD, (nextIndex + super.K + 1) * 4096, SEEK_SET);    
       //fseek(tempFD, (k+1)* 4096 + 1, SEEK_SET);
       lseek(fd, 4096 * nextIndex, SEEK_SET); //go to start of next block
       //decide later? do sme whiteboarding. or if you are super sure, choose one

       bytesTraversed = 0; 
   }
   return -1; //did not find the dir
}
   
static int fat_opendir(const char* path, struct fuse_file_info* fi){
    if (strcmp(path, "/") == 0){

        //make sure we return the offset
        fi->fh = lseek(fd, super.rootBlock * 4096, SEEK_SET); 
        
        return 0;
    }else if ( fat_opendir_helper(path, fi, 1, NULL) == -1){

        return -ENOENT;
    }else {

        return 0;
    }
}
//opens the dir with the path.
//more specifically, fat opendir helper (/newfolder ...) will give fi->fh the offset in harddrive for metadata of /newFolder
//startBlockReturn, can return block number for that directory
   
int fat_opendir_helper(const char* path, struct fuse_file_info* fi, int flag, int* startBlockReturn ){
    
    char * tempStr = malloc(strlen(path) + 1);
    memset(tempStr, 0, strlen(path) + 1);
    strcpy(tempStr, path); //get a temp copy of path

    char * pch, *temp;
    pch = strtok(tempStr, "/");

    struct fatDirectory tempDir;
    int startBlock = super.rootBlock; 
    while(pch != NULL){
        if ( ((temp = strtok(NULL, "/")) == NULL) && flag == 0){
            startBlock = findDirectory(fatPointer, pch, startBlock, 0);
        } else
            startBlock = findDirectory(fatPointer, pch, startBlock, 1);
        if ( startBlock == -1){
            fi->fh = -1;
            fi->flags = -1;
            return -1;
        }
        pch = temp;
    }

    //change formula?
    //fseek(fd, (startBlock + super.K + 1 ) * 4096, SEEK_SET); 
    free(tempStr);
    int offSetOfFoundFile = lseek(fd, (startBlock * 4096), SEEK_SET);
    fi->fh = offSetOfFoundFile;

    if (startBlockReturn != NULL){
        *startBlockReturn = startBlock;
    }
    return 0;
}
//fileHandleIN to start seek, must be at the place where fatDirectory starts, the fatDirectory Information where fileHandleIN points to, fileHandleOUT points to free space in block for metadata placement 
//int fDir is the startblock of the directory we are looking at
static int findFreeSpaceInDirectory(int fileOffset, int startBlock, int* fileOffsetOut){

   int bytesTraversed = 0;

   struct fatDirectory tempFdir;
   memset(&tempFdir, 0, sizeof(struct fatDirectory));
   
       
   int nextIndex = startBlock; 
   lseek(fd, startBlock * 4096, SEEK_SET);

   while( (nextIndex != -2)){

       while (bytesTraversed < 4096 ){
            
          // bytesTraversed += fread(&tempFdir, sizeof(struct fatDirectory), 1, tempFD);
           bytesTraversed += read(fd, &tempFdir, sizeof(struct fatDirectory));
           
           if( tempFdir.fileName[0] == '\0' ){
               //if succesfully read in a fatDirectory, and its name is empty, must be a free space to place it.
                //already pointing to the end of the free struct 
               int sz = sizeof(struct fatDirectory);
               
               //fseek(tempFD, SEEK_CUR, -sz);
               *fileOffsetOut  = lseek(fd, -sz, SEEK_CUR);
               //seek back to the start of the free struct
               return 0; 
           }
           
       }//at the end of this loop, we traversed 4096, so go to next block
       nextIndex = fatPointer[nextIndex];
       //make sure all indexes referenced from start of root block.
       //fseek(tempFD, (nextIndex + super.K + 1) * 4096, SEEK_SET);    
       lseek(fd, nextIndex * 4096, SEEK_SET);
       //fseek(tempFD, (k+1)* 4096 + 1, SEEK_SET);
       //fseek(tempFD, 4096 * nextIndex, SEEK_SET);
       //decide later? do some whiteboarding. or if you are super sure, choose one

       bytesTraversed = 0; 
   }
   //if no empty bytes found, then no free blocks, so return -1 to the caller. caller will want to call findFreeFATEntry and allocate another block and write to it. 
    int newEntry = findFreeFATEntry(fatPointer);
    if (newEntry != -1){

        fatPointer[nextIndex] = newEntry;
        fatPointer[newEntry] = -2;
        writeFatTableToDisk();
        return newEntry;
    }else if (newEntry == -1){
        return -1; 
    }

    return -1; //did not find the dir
}
//return an index which is freee in fat table

int findFreeFATEntry(uint32_t* fatPtr){
    int i = super.K + 1; //skip fat and super block
    for (; i < super.K * 1024; i++){ //there are 1024 entries in each fat block. this times k, gives us all entries in fat table

        if(fatPointer[i] == 0){

            return i;
        }
    }
    return -1;
}
//CALL THIS FUNCTION WHENVER FATPOINTER IS AN L VALUE

void writeFatTableToDisk(){
    //seek to beginning of fat block.
    //save current offset.
    int offset = lseek(fd, 0, SEEK_CUR); //save current offset 
    lseek(fd, 4096, SEEK_SET);
    //write fat table from memory to disk.
    //size of fat table is super.K * 4096 bytes
    write(fd, fatPointer, super.K * 4096);
    lseek(fd, offset, SEEK_SET); //go back to original

}
static int fat_mkdir(const char* path, mode_t mode){

    struct stat temp;
    memset(&temp, 0, sizeof(temp));

    struct fuse_file_info fi;
   
    printf("MKDIR CALLED\n");
    fflush(stdout);

   char * pathWithoutFileName = malloc(strlen(path));
   char pathWithName[24];
   memset(pathWithName, 0, 24);
   memset(pathWithoutFileName, 0, strlen(path));

   //get the path, and split filename. ex /usr/local/file.txt, and /usr/local/ and file.txt
   cutLastPath(pathWithoutFileName, path, pathWithName);
   
   //look up the directory and get its startblock
   int startBlockOfDirectory = 0; 
  
   fat_opendir_helper(pathWithoutFileName, &fi, 1, &startBlockOfDirectory); 

   //after lookup and startblock found, traverse to find free space
   int whereToWriteMetadata = 0;
   int newEntry = findFreeSpaceInDirectory(fi.fh, startBlockOfDirectory, &whereToWriteMetadata);


   //lookup table to get a free block to assign to the directory struct
   struct fatDirectory newDir; //create new metadata
   memset(&newDir, 0, sizeof(struct fatDirectory));

   newDir.startBlock = findFreeFATEntry(fatPointer);
   
   fatPointer[newDir.startBlock] = -2; //since new dir is empty, can mark as end block
   writeFatTableToDisk();
   
   //fill in directory metadata
   strcpy(&(newDir.fileName[0]), pathWithName);
   newDir.creationTime = time( NULL );
   newDir.accessTime = time( NULL );
   newDir.modificationTime = time ( NULL );

   newDir.fileLength = 4096;
   newDir.flags = 1;
   newDir.accessTime = time ( NULL );
   newDir.modificationTime = time ( NULL );

   //write metdata to disk
   if (newEntry > 0){ //did not find free block so find another fat block

       lseek(fd, newEntry * 4096, SEEK_SET); //newEntry is the fat block
   }else if (newEntry == 0){
    //where to write metadata will be the offset of where we found free space (end of free space - size of free space)
    //do nothin here. proper programming style for this project is to seek before ANY write

       lseek(fd, whereToWriteMetadata, SEEK_SET); //where to write metadata will 
   }else if (newEntry == -1){

       return -1; //errror
   }

   write (fd, &newDir, sizeof(struct fatDirectory));
   return 0;
}

// Remove file from directory
static int fat_unlink(const char* path){
    printf("UNLINK/RM CALLED\n");
    fflush(stdout);

    struct fatDirectory tempFdir;
    int offset = 0; 

    memset(&tempFdir, 0, sizeof(struct fatDirectory));
    char *temp = malloc(strlen(path) + 1);
    memset(temp, 0, strlen(path) + 1);

    strcpy(temp, path);
    int success = getDirMetadata(temp, &tempFdir, 0, &offset);
    if (success == -1)
        return -1; // could not find directory
    int fileBlock = tempFdir.startBlock; // seek to beg of file block
    lseek(fd, offset, SEEK_SET);

    struct fatDirectory emptyDir;
    memset(&emptyDir, 0, sizeof(emptyDir));
    write(fd, &emptyDir, sizeof(struct fatDirectory)); // erase entry
    while (fileBlock != -2){
       lseek(fd, fileBlock * 4096, SEEK_SET);
        
       for (int i = 0; i < 4096; i++)
            write(fd, "\0", 1);
       int tempBlock = fatPointer[fileBlock];
       fatPointer[fileBlock] = 0;
       fileBlock = tempBlock;
    }
    writeFatTableToDisk();
    return 0;
}

static int fat_rmdir(const char* path){

    struct fatDirectory tempFdir;
    int offset = 0, bytesTraversed = 0; 

    memset(&tempFdir, 0, sizeof(struct fatDirectory));
    char *temp = malloc(strlen(path) + 1);
    memset(temp, 0, strlen(path) + 1);
    strcpy(temp, path);
    int success = getDirMetadata(temp, &tempFdir, 1, &offset);
    //if (success == -1)
    //    return -1; // could not find directory
    //not needed, getattr already called

    fflush(stdout);
    int dirBlock = tempFdir.startBlock; // seek to beg of directory block
    int startBlock = dirBlock; // keep track of beginning directory block for later
    // check if directory has contents
    while (dirBlock != -2){
        lseek(fd, dirBlock * 4096, SEEK_SET);
        while (bytesTraversed < 4096){
            read(fd, &tempFdir, sizeof(struct fatDirectory));
            // if file name exists and is not the parent or current directory
            if (tempFdir.fileName[0] != '\0') 
                return -1; // directory not empty
       //&& strcmp(&tempFdir.fileName[0], "..") && strcmp(&tempFdir.fileName[0], "."))
            bytesTraversed += sizeof(struct fatDirectory);
        }
        bytesTraversed = 0;
        dirBlock = fatPointer[dirBlock];
    }
    // directory is empty, can now delete it from memory and FAT table
    dirBlock = startBlock;
    while (dirBlock != -2) {
        lseek(fd, dirBlock * 4096, SEEK_SET);
        for(int i = 0; i < 4096; i++)
            write(fd, "\0", 1);
        int tempBlock = fatPointer[dirBlock];
        fatPointer[dirBlock] = 0;
        dirBlock = tempBlock;
    }
    writeFatTableToDisk();
    lseek(fd, offset, SEEK_SET); // Seek to and delete directory entry in parent directory block

    memset(&tempFdir, 0, sizeof(tempFdir)); 
    write(fd, &tempFdir, sizeof(struct fatDirectory));
    return 0;
}

static int fat_truncate(const char* path, off_t size){
	printf("fat truncate!");
	fflush(stdout);

	struct fuse_file_info fiTemp;
	struct fuse_file_info* fi = &fiTemp;
	
	int res = -1;
    	fat_opendir_helper(path, fi, 0, NULL);
	lseek(fd, fi->fh, SEEK_SET);
	struct fatDirectory tempDir;
	int startBlock = fi->fh / 4096;
	int offSetTemp = 0;
	getDirMetadata(path, &tempDir, 0, &offSetTemp);
	int fileSize = tempDir.fileLength;
	int blocksToGo = fileSize / 4096;
	int offsetToGo = fileSize % 4096;
	
	//lseek(fd, startBlock * 4096, SEEK_SET);
	//lseek(fd, offsetToGo, SEEK_CUR);          //go to the end of the file
	
	
	if (fileSize < size){          // file size is smaller than the size so we extend it and fill it with zeros


		int diffSize = size - fileSize;
		if ((offsetToGo + diffSize) > 4096){      //when the block size is not big enough
			//write the rest of the block
			for (int i=0; i<blocksToGo; i++){
				
				if (fatPointer[startBlock] == -2){ //here startBlock will be the index where has -2. last block in file
					lseek(fd, startBlock * 4096, SEEK_SET);
					lseek(fd, offsetToGo, SEEK_CUR); // go to offset in last block
					for (int w = 0; w < 4096 - offsetToGo; w++){
						write(fd, "\0", 1);
					}
					int numBlocksToAdd = (diffSize - offsetToGo)/4096;
					for (int j=0; j<numBlocksToAdd; j++) {
						int newEntry = findFreeFATEntry(fatPointer);
						fatPointer[startBlock] = newEntry;
						fatPointer[newEntry] = -2;
						lseek(fd, newEntry*4096, SEEK_SET);
						for (int w = 0; w < 4096; w++){
							write(fd, "\0", 1);
						}
						startBlock = newEntry;
					}
				}
				startBlock = fatPointer[startBlock];				
			}
		}
		else {
			for (int i=0; i<blocksToGo; i++){
				
				if (fatPointer[startBlock] == -2){ //here startBlock will be the index where has -2. last block in file
					lseek(fd, startBlock * 4096, SEEK_SET);
					lseek(fd, offsetToGo, SEEK_CUR); // go to offset in last block	
		
					for (int w = 0; w < 4096 - offsetToGo; w++){
						write(fd, "\0", 1);
					}
				}
				startBlock = fatPointer[startBlock];	
			}
			
		}
		writeFatTableToDisk();
		res = 0;
		return res;;
	}
	else if (fileSize > size) {   // file size is larger than the size so we truncate it
		int truncSize = fileSize - size;
		if ((offsetToGo - truncSize) >= 0) {
			for (int i=0; i<blocksToGo; i++){
				if (fatPointer[startBlock] == -2){ //here startBlock will be the index where has -2. last block in file
					lseek(fd, startBlock * 4096, SEEK_SET);
					lseek(fd, offsetToGo, SEEK_CUR); // go to offset in last block
					lseek(fd, -truncSize, SEEK_CUR);
					for (int w = 0; w < truncSize; w++){
						write(fd, "\0", 1);
					}
				}
				startBlock = fatPointer[startBlock];	
			}
		}
		else {
			
			for (int i=0; i<blocksToGo; i++){
				if (fatPointer[startBlock] == -2){ //here startBlock will be the index where has -2. last block in file
					lseek(fd, startBlock * 4096, SEEK_SET);
					for (int w = 0; w < 4096; w++){
						write(fd, "\0", 1);
					}
					fatPointer[startBlock] = 0;
					break;
				}
				
				startBlock = fatPointer[startBlock];	
			}
			truncSize = truncSize - offsetToGo;
			int numBlockToTrunc = truncSize/4096;
			int numOffsetToTrunc = truncSize%4096;
			for (int k=0; k < (blocksToGo - numBlockToTrunc); k++){ //where to start continuous block truncate
				//goto previous block
				//set block = 0
				//goto the start of the block
				startBlock = fatPointer[startBlock];
			}
			int blockToGoTemp =0;
			for (int a = 0; a < numBlockToTrunc; a++){ //once found where to start connt. block truncate, truncaate it
				lseek(fd, startBlock*4096, SEEK_SET);
				for (int w = 0; w < 4096; w++){
						write(fd, "\0", 1);
				}
				blockToGoTemp = fatPointer[startBlock];
				fatPointer[startBlock] = 0;
				startBlock = blockToGoTemp;
			}
			truncSize = truncSize - 4096*numBlockToTrunc;
			startBlock = fi->fh / 4096; //beginning
			for (int q = 0; q < blocksToGo - numBlockToTrunc - 1; q++){	//go to last block
			//last block is minus 1 of how many blocks we traversed to get to the start of the continuous block truncate
				
				startBlock = fatPointer[startBlock];
			}
			fatPointer[startBlock] = -2;
			lseek(fd, startBlock * 4096, SEEK_SET);
			lseek(fd, 4096-truncSize, SEEK_CUR);
			for (int w = 0; w < truncSize; w++){
				write(fd, "\0", 1);
			}
		
		}
		writeFatTableToDisk();
		res = 0;
		return res;
	}
	else if (fileSize == size){   // file size is same as the size so we just return
		res = 0;
		return res;
	}
	return res;
}

static int fat_write(const char* path, const char *buf, size_t size, off_t offset, struct fuse_file_info* fi){
    int res = 0;
	fat_opendir_helper(path, fi, 0, NULL);
	int startBlock = fi->fh/4096; //get the startBlock number

	lseek(fd, fi->fh, SEEK_SET);
	int blocksToGo = offset / 4096;  //number of blocks we need to go

	int offsetToGo = offset % 4096;  //offset from the block
	for (int i=0; i<blocksToGo; i++){
		startBlock = fatPointer[startBlock];
		if (startBlock == -2){
			return -ESPIPE;          // return error when the offset goes beyond EOF
		}
	}	

	lseek(fd, startBlock * 4096, SEEK_SET);
	lseek(fd, offsetToGo, SEEK_CUR);

	res = write (fd, buf, size);
    printf("fat write!");
	fflush(stdout);
    if (res == 0){
        res = -EIO;
    }
	return res;
}

static int fat_utimens(const char* path, const struct timespec ts[2] ){

    printf("UTIMENS");
    struct fuse_file_info fi;
    struct fatDirectory tempDir;
    int offset = 0;
    if (getDirMetadata(path, &tempDir, 0, &offset) != -1){

        lseek(fd, offset, SEEK_SET);
        tempDir.accessTime = ts[0].tv_sec;
        tempDir.modificationTime = ts[1].tv_sec;
        write(fd, &tempDir, sizeof(tempDir));
        
    }else if (getDirMetadata(path, &tempDir, 1, &offset) != -1){

        lseek(fd, offset, SEEK_SET);
        tempDir.accessTime = ts[0].tv_sec;
        tempDir.modificationTime = ts[1].tv_sec;
        write(fd, &tempDir, sizeof(tempDir));
    
    }else {
        return -ENOENT;
    }
    return 0;
}
static int fat_getattr(const char *path, struct stat *stbuf)
{

    printf("Get attr\n");
    fflush(stdout);

	int res = 0;
    if ( (strcmp(path, "/") == 0) || (strcmp(path, ".") == 0) ){
        //for root and current directory, return statistics

        stbuf->st_atime = time (NULL); //access time 
        stbuf->st_mtime = time (NULL); //modification time
        stbuf->st_ctime = time (NULL); //status change
        stbuf->st_mode = S_IFDIR | S_IRWXU |S_IRWXG |S_IRWXO;
        stbuf->st_blksize = 4096;
        stbuf->st_size = 4096;
        res = 0; //return val set to 0 for found
    }else {
        //for non root, lets search

        struct fuse_file_info fiTemp;

        if (fat_opendir_helper(path, &fiTemp, 1, NULL) == -1){
            
            if (fat_opendir_helper(path, &fiTemp, 0, NULL) == -1){
                res = -ENOENT;
            }else {

               
               struct fatDirectory tempDir;
               memset(&tempDir, 0, sizeof(struct fatDirectory));

               lseek(fd, fiTemp.fh, SEEK_SET);
               read(fd, &tempDir, sizeof(struct fatDirectory)); 
               stbuf->st_atime = time ( NULL );
 
               stbuf->st_atime = tempDir.accessTime;
               stbuf->st_mtime = tempDir.modificationTime;
               stbuf->st_ctime = time(NULL);
               stbuf->st_mode = S_IFREG | S_IRWXU |S_IRWXG |S_IRWXO;
               stbuf->st_blksize = 4096;
               stbuf->st_size = 4096; //need to change
               res = 0;
               //important when implementing read, and write, here we need to set stbuf->st_size to its correct value
            }

        }else {

           struct fatDirectory tempDir;
           memset(&tempDir, 0, sizeof(struct fatDirectory));
           lseek(fd, fiTemp.fh, SEEK_SET);
           read(fd, &tempDir, sizeof(struct fatDirectory)); 
              
           stbuf->st_atime = tempDir.accessTime;
           stbuf->st_mtime = tempDir.modificationTime;
           stbuf->st_ctime = time(NULL);
           stbuf->st_mode = S_IFDIR | S_IRWXU |S_IRWXG |S_IRWXO;
           stbuf->st_blksize = 4096; //need to change
           stbuf->st_size = 4096;
           res = 0;
        }
    }
    
	        //write function later, to set st_size    
		//stbuf->st_nlink = 1;
		//stbuf->st_size = strlen(options.contents);
        //come back here to fill this out. to set st_size, and st_blocks

	return res;
}

static int fat_readdir_ex(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi
			)
{
    //current directory to read already stored in fh

   fat_opendir(path, fi);
   int currentDirOffset = fi->fh;
        
   struct fatDirectory tempFdir;
   struct stat tempStat;
   memset(&tempFdir, 0, sizeof(struct fatDirectory));
   memset(&tempStat, 0, sizeof(struct stat));

   tempStat.st_mode = S_IFDIR | S_IRWXU |S_IRWXG |S_IRWXO;
   
   tempStat.st_blksize = 4096; //need to change

   
   int bytesTraversed = 0;
   int nextIndex = currentDirOffset / 4096; //from our offset, get the block 
   //setup read head to seek the folderf
   lseek(fd, nextIndex * 4096, SEEK_SET);
    //iterate through directory and get all dir entries
   while(nextIndex != -2){
     
       while (bytesTraversed < 4096 ){
            
           bytesTraversed += read(fd, &tempFdir, sizeof(struct fatDirectory));
           
           if ( tempFdir.fileName[0] != '\0'  ){
               //metadata found, fill in stat struct
               tempStat.st_atime = tempFdir.accessTime;
               tempStat.st_ctime = tempFdir.creationTime;
               tempStat.st_mtime = tempFdir.modificationTime;
               tempStat.st_size = tempFdir.fileLength; //default value, change later

               filler(buf, &tempFdir.fileName[0], &tempStat, 0);
           }

       }//at the end of this loop, we traversed 4096, so go to next block
       nextIndex = fatPointer[nextIndex];
       //make sure all indexes referenced from start of root block.
       lseek(fd, 4096 * nextIndex, SEEK_SET);
       bytesTraversed = 0; 
   }
    printf("Readdir!");
    fflush(stdout);
    return 0;
}


static int fat_open(const char *path, struct fuse_file_info *fi)
{
    //call fat_opendir
    if (fat_opendir_helper(path, fi, 0, NULL) == -1){
        fi->direct_io = 1;
       
        return -ENOENT;
    }
    
	return 0;
}

static int fat_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	int res = 0;
	fat_opendir_helper(path, fi, 0, NULL);
	int startBlock = fi->fh/4096; //get the startBlock number
	lseek(fd, fi->fh, SEEK_SET);
	int blocksToGo = offset / 4096;  //number of blocks we need to go
	int offsetToGo = offset % 4096;  //offset from the block
	for (int i=0; i<blocksToGo; i++){
		startBlock = fatPointer[startBlock];
		if (startBlock == -2){
			return 0;                // return 0 if the given offset was at or beyond EOF
		}
	}	
	lseek(fd, startBlock * 4096, SEEK_SET);
	lseek(fd, offsetToGo, SEEK_CUR);
	res = read (fd, buf, size);
    printf("fat read!");
	fflush(stdout);
	return res;
}
//returnHere is a zero initialized char array.
//returnHere will contain the last path
void cutLastPath(char * returnHere, const char * source, char * lastPathName){

    int posEnd = strlen(source) - 1;
    while(source[posEnd] != '/'){
        posEnd--;
    } //find posEnd from tail to first slash
    if (posEnd < 0){

        posEnd = 0;
    }
    int i = 0;
    for (; i <= posEnd; i++){
        returnHere[i] = source[i];
    }
    int j = 0;
    for (i = posEnd+1; i < strlen(source); i++){
       
        lastPathName[j] = source[i];
        j++;
    }
    if (strlen(returnHere) == 1){
        returnHere[strlen(returnHere)] = 0;
    }else
        returnHere[strlen(returnHere) - 1] = 0;

    printf("returnHere: %s\nlastPathName: %s", returnHere, lastPathName);
}


static int fat_create(const char* path, mode_t mode, struct fuse_file_info* fitemp ){
    printf("Create\n");
    fflush(stdout);
    //need to cut off last part of the path
    struct fuse_file_info fi;
    int startBlockOfDirectory = 0;
    printf("CREATE CALLED\n");
    fflush(stdout);

    char * pathWithoutFileName = malloc(strlen(path) + 1);
    char pathWithName[24];
    memset(pathWithName, 0, 24);
    memset(pathWithoutFileName, 0, strlen(path) + 1);
    //get the path, and split filename. ex /usr/local/file.txt, and /usr/local/ and file.txt
    cutLastPath(pathWithoutFileName, path, pathWithName);
    //look up the directory and get its startblock 
    fat_opendir_helper(pathWithoutFileName, &fi, 1, &startBlockOfDirectory);

    //after lookup and startblock found, traverse to find free space
    int whereToWriteMetadata = 0;
    int newEntry = findFreeSpaceInDirectory(fi.fh, startBlockOfDirectory, &whereToWriteMetadata);

    //lookup table to get a free block to assign to the directory struct
    struct fatDirectory newDir; //create new metadata
    memset(&newDir, 0, sizeof(struct fatDirectory));

    newDir.startBlock = findFreeFATEntry(fatPointer);
   
    fatPointer[newDir.startBlock] = -2; //since new dir is empty, can mark as end block
    writeFatTableToDisk();
   
    //fill in directory metadata
    strcpy(&(newDir.fileName[0]), pathWithName);
    newDir.creationTime = time( NULL );
    newDir.fileLength = 4096;
    newDir.flags = 0;
    newDir.accessTime = time ( NULL );
    newDir.modificationTime = time ( NULL );

    //write metdata to disk
    if (newEntry > 0){ //did not find space in block but found another free fat block
       lseek(fd, newEntry * 4096, SEEK_SET); //newEntry is the fat block
    }else if (newEntry == 0)
    //where to write metadata will be the offset of where we found free space (end of free space - size of free space)
    //do nothin here. proper programming style for this project is to seek before ANY write

       lseek(fd, whereToWriteMetadata, SEEK_SET); //where to write metadata will 
    else if (newEntry == -1)
       return -1; //error

    write (fd, &newDir, sizeof(struct fatDirectory));

    return 0;

}

static int fat_access(const char* path, int m){
   
    struct stat stb;
    struct fuse_file_info fi;
    if (fat_getattr(path, &stb) == -ENOENT){
        return -ENOENT;
    }
    return 0;
}
static int fat_rename(const char* from, const char* to){

    return 0;
}

static int fat_statfs(const char* path, struct statvfs* stbuf){

    stbuf->f_namemax = 24;
    //stbuf->f_frsize = 1;
    stbuf->f_bsize = 4096;
    stbuf->f_flag = ST_NOSUID;
    return 0;
}

static int fat_release(const char* path, struct fuse_file_info* fi){

     return 0;
}

static int fat_releasedir(const char* path, struct fuse_file_info* fi){

    return 0;
}

static int fat_flush(const char* path, struct fuse_file_info* fi){

    return 0;
}
static int fat_mknod(const char *path, mode_t mode, dev_t rdev){

    printf("MKNOD");
    return 0;
}

static struct fuse_operations fat_oper = {
    .create     = fat_create,   
	   .getattr	= fat_getattr,  
	   .open		= fat_open, 
	   .read		= fat_read, 
    .readdir    = fat_readdir_ex, 
    .opendir    = fat_opendir,  
    .mkdir      = fat_mkdir,   
    .unlink     = fat_unlink,   
    .rmdir      = fat_rmdir,    
    .truncate   = fat_truncate, 
    .write      = fat_write,    
    .utimens   = fat_utimens,   
    .access     = fat_access,   
    .rename     = fat_rename,   
    .statfs     = fat_statfs,       
    .release    = fat_release,      
    .releasedir = fat_releasedir,   
    .flush      = fat_flush,    
    .mknod      = fat_mknod     
};

int main(int argc, char *argv[])
{
    if (argc > 1){

        fd = open("hardDrive", O_RDWR);
        if (fd == -1){
            printf("need a file called \"hardDrive\" use create program to make it");
        }
        
        read(fd, &super, sizeof(struct superBlock)); //read superblock into memory

        fatPointer = malloc(super.K * 4096); //load fat block into memory first allocate.
        
        lseek(fd, 4096, SEEK_SET);          //seek to the fat block
        //read in the fat block to malloced memory`
        read(fd, fatPointer, super.K * 4096);

        //after reading, we are at end of fat block  at beginning of root block

        
        //in create, make sure  the first 2 fat pointers to -1, as it points to super and fat blocks

    	return fuse_main(argc, argv, &fat_oper, NULL);

      }else {

          printf("pass more arguments (did you pass file for fs??");
          return 0;
      }

}
