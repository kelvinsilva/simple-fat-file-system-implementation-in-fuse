# README #

Role Lead: Developer and Designer

Description: Implementation of a simple file system using the classic File Allocation Table architecture. 

Implemented using libfuse on FreeBSD 10. Can create directories, and subdirectories, and read write files

Technology: File-systems In User-space (FUSE), C, UNIX


### What is this repository for? ###

Simple file system using File Allocation Table architecture (4k blocks).

Libfuse, FreeBSD 10. 

Two programs are in this project. First program is to create and initialize a fake hard drive file. 
Create.c --to make fake hdd file
main.c -- file system implementation

The mounted file system will read this fake hard drive file and write records and data to it.

* Libfuse 2.9.6


### How do I get set up? ###

Read design document to install libfuse and setup development environment for execution of project.

### Contribution guidelines ###

Kelvin Silva, CMPS 111, University of California Santa Cruz


### Who do I talk to? ###

kelvinsilva747@gmail.com