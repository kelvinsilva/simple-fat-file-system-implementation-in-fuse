fatFS: main.c create.c

	cc -D_FILE_OFFSET_BITS=64 -I/usr/local/include/fuse  -pthread -L/usr/local/lib -lfuse -lrt -g main.c -o fatFS 

	cc create.c -o create

clean:
	rm fatFS
	rm create
